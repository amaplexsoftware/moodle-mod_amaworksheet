Amanote Worksheet
=================
Amanote Worksheet module for Moodle (http://moodle.org/)

A worksheet is a file with questions or exercises for students. Amanote's Worksheet plugin allows students to answer the questions directly on or beside the PDF and teachers to retrieve the students' answers.

In order for the module to work properly, please check that the following requirements are met on your Moodle site:
* Web services are enabled (Site administration > Advanced feature)
* Moodle mobile web service is enabled (Site administration > Plugins > Web services > External services)
* REST protocol is activated (Site administration > Plugins > Web services > Manage protocols)
* Capability *webservice/rest:use* is allowed for *authenticated users* (Site administration > Users > Permissions > Define Roles > Authenticated Users > Manage roles)
